import Ember from 'ember';

export default Ember.Controller.extend({
  options: ['one', 'two', 'three', 'four', 'five'],
  selected: [],

  actions: {
    foo(value) {
      this.set('selected', value);
    }
  }
});