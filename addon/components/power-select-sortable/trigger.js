import layout from '../../templates/components/power-select-sortable/trigger';
import MultipleTriggerComponent from 'ember-power-select/components/power-select-multiple/trigger';

export default MultipleTriggerComponent.extend({
  layout,
  // Actions
  actions: {
    reorderSelection(reorderedOptions /*, draggedModel */) {
      this.get('select.actions.select')(reorderedOptions);
    }
  }
});
