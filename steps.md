run

```
ember addon ember-power-select-sortable
ember install ember-cli-sass
ember install ember-power-select@0.8.0-beta.2
```


Move "ember-cli-htmlbars" to the "dependencies" section

Add
```hbs
{{#power-select-multiple options=options selected=selected onchange=(action (mut selected)) as |opt|}}
  {{opt}}
{{/power-select-multiple}}
````
to the application.hbs

Create `tests/dummy/app/controllers/application.js`
```js
import Ember from 'ember';

export default Ember.Controller.extend({
  options: ['one', 'two', 'three', 'four', 'five']
});
```

# initial state!
```
ember install ember-sortable
```